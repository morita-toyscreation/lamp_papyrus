#!/bin/sh

PHP=$(which php)
MYSQL=$(which mysql)
USERNAME=$(whoami)
SCRIPT_DIR=$(cd $(dirname $0); pwd)
YMD=$(date "+%Y%m%d")
DATABASE="lamp_papyrus"
TABLE="fc2_blog_rss"

# 2週間以上の古いデータを削除（毎日0時に実行）
echo "* 0 * * * ${MYSQL} --defaults-extra-file=${SCRIPT_DIR}/dbaccess.cnf --database=${DATABASE} --execute=\"DELETE FROM ${TABLE} WHERE (created_at <= DATE_SUB(CURDATE(), INTERVAL 2 WEEK))\"" >> /tmp/crontab.temp

# FC2BLOG RSSデータ保存（5分毎に実行）
echo "*/5 * * * * ${PHP} ${SCRIPT_DIR}/save.php >> ${SCRIPT_DIR}/logs/cron.${YMD}.log" >> /tmp/crontab.temp

if crontab -u ${USERNAME} /tmp/crontab.temp
then
    echo "crontab install is done successfully."
else
    echo "crontab install is failed."
fi
rm /tmp/crontab.temp
