CREATE SCHEMA IF NOT EXISTS `lamp_papyrus` DEFAULT CHARACTER SET utf8 ;

use lamp_papyrus;

DROP TABLE IF EXISTS `fc2_blog_rss`;

CREATE TABLE `fc2_blog_rss` (
  `url` VARCHAR(255) NOT NULL COMMENT 'URL',
  `date` DATETIME NOT NULL COMMENT '日付',
  `user_name` VARCHAR(255) COMMENT 'ユーザー名',
  `server_num` INT(10) unsigned NOT NULL COMMENT 'サーバー番号',
  `entry_no` INT(10) unsigned NOT NULL COMMENT 'エントリーNo.',
  `title` TEXT NOT NULL COMMENT 'タイトル',
  `description` TEXT NOT NULL COMMENT '概要',
  `status` TINYINT(3) UNSIGNED NULL DEFAULT 1,
  `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`url`),
  KEY `date_idx` (`date`),
  KEY `user_name_idx` (`user_name`),
  KEY `server_num_idx` (`server_num`),
  KEY `entry_no_idx` (`entry_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='FC2ブログRSS';
