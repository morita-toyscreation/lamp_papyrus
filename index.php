<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/inc.php';
require_once __DIR__ . '/lib/helper.php';
require_once __DIR__ . '/lib/fc2_blog_rss_query.php';

use Lib\Helper;


if (empty($_SERVER['REQUEST_URI'])) {
    exit;
}

$error_messages = array();

if (!empty($_GET["submit"])) {

    $request = array(
        "date" => $_GET["date"],
        "url" => $_GET["url"],
        "user_name" => $_GET["user_name"],
        "server_num" => $_GET["server_num"],
        "entry_no" => $_GET["entry_no"],
    );

    // 入力データバリデーション
    $error_messages = Helper::form_validation($request);

    if (empty($error_messages)) {
        $_SESSION["request"] = $request;
    }

} else {
    $request = $_SESSION["request"];
}

$date = $request["date"];
$url = $request["url"];
$user_name = $request["user_name"];
$server_num = $request["server_num"];
$entry_no = $request["entry_no"];

$page = 1;
if (!empty($_GET["page"])) {
    $page = $_GET["page"];
}

if (empty($error_messages)) {
    // MySQLから検索条件に沿ってFC2 BLOG RSSデータを取得
    $fc2_blog_rss_query = new Fc2BlogRssQuery();
    $rss_data = $fc2_blog_rss_query->get_rss($date, $url, $user_name, $server_num, $entry_no, $page);
} else {
    $rss_data = array(
        "status" => FALSE,
        "rows" => array(),
        "count" => 0,
    );
}

$paging_url = "/?page=%d";
if (!empty($request)) {
    $paging_url = "/?" . http_build_query($request) . "&page=%d";
}

$paging_data = array(
    "total_row_count" => $rss_data["count"],
    "page_row_count" => PAGE_ROW_COUNT,
    "current_page" => $page,
    "url" => $paging_url
);

// Smarty開始
$smarty = new Smarty();
$smarty->template_dir = __DIR__ . '/templates/';
$smarty->escape_html = true;

$smarty->assign("request", $request);
$smarty->assign("error_messages", $error_messages);
$smarty->assign("rss_data", $rss_data);
$smarty->assign("paging_data", $paging_data);

$smarty->display('index.tpl');
