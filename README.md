# 導入手順書

## 動作環境

本プログラムはPHP7、MySQL8前提で作成しました。  
動作環境はPHP7、MySQL8を推奨

## インストール

1. BitBucket からリポジトリダウンロード
https://bitbucket.org/morita-toyscreation/lamp_papyrus/src/master/

2. ダウンロードしたプログラムZIPファイルを解凍します。

3. 解凍したファイル全てを、SFTPクライアントを使ってサーバーの公開フォルダへアップロードします。

## ライブラリのインストール

Smartyをcomposer経由でインストール 

```
$ composer install
```

## パーミッションの変更

templates_c、logsのパーミッションを777に変更

```
$ chmod 777 templates_c
$ chmod 777 logs 
```

## MySQL設定

ユーザー追加

```
$ mysql -u root -p < sql/grant.sql
```

初期データベース、テーブル追加

```
$ mysql -u root -p < sql/lamp_papyrus.sql
```

## cron設定

FC2BLOG RSSデータ保存（5分毎に実行）及び2週間以上の古いデータを削除（毎日0時に実行）を設定

登録スクリプト実行

```
$ sh crontab.sh
```

これで利用できるようになります。