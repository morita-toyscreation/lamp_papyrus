<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/inc.php';
require_once __DIR__ . '/lib/fc2_blog_rss_load.php';
require_once __DIR__ . '/lib/fc2_blog_rss_query.php';

$date = date("Y-m-d h:i:s");

// FC2BLOG RSS取得
$fc2_blog_rss_load = new Fc2BlogRssLoad();
$load_result = $fc2_blog_rss_load->get_rss();

print(sprintf("[%s] %s \n", $date, $load_result["message"]));

if ($load_result["status"] !== TRUE) {
    error_log($load_result["message"]);
    exit();
}

// FC2BLOG RSS DB登録
$fc2_blog_rss_query = new Fc2BlogRssQuery();
$query_result = $fc2_blog_rss_query->save_rss($load_result["rss_data"]);

print(sprintf("[%s] %s \n", $date, $query_result["message"]));

if ($query_result["status"] !== TRUE) {
    error_log($query_result["message"]);
    exit();
}
