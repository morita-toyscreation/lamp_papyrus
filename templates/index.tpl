<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title></title>
    <style>
        table, td, th {
            border-collapse: collapse;
            border:1px solid #333;
            word-break: break-all;
        }
        .error {
            color: red;
        }
    </style>
</head>
<body>

<form action="/" method="get">
    <p>
        {if $error_messages.date}<span class="error">{$error_messages.date}</span><br />{/if}
        <label>日付：<input type="date" name="date" value="{$request.date}"></label>
    </p>
    <p>
        {if $error_messages.url}<span class="error">{$error_messages.url}</span><br />{/if}
        <label>URL：<input type="url" name="url" size="40" value="{$request.url}"></label>
    </p>
    <p>
        {if $error_messages.user_name}<span class="error">{$error_messages.user_name}</span><br />{/if}
        <label>ユーザー名：<input type="text" name="user_name" size="40" value="{$request.user_name}"></label>
    </p>
    <p>
        {if $error_messages.server_num}<span class="error">{$error_messages.server_num}</span><br />{/if}
        <label>サーバー番号：<input type="number" name="server_num" size="40" value="{$request.server_num}"></label>
    </p>
    <p>
        {if $error_messages.entry_no}<span class="error">{$error_messages.entry_no}</span><br />{/if}
        <label>エントリーNo（エントリーNo.が○○以上）：<input type="number" name="entry_no" size="40" value="{$request.entry_no}"></label>
    </p>
    <p><input type="submit" name="submit" value="検索"></p>
</form>

{if $rss_data.status == FALSE}
<span>FC2BLOG RSS取得失敗</span>
{else}

{$total_page = ceil($paging_data.total_row_count / $paging_data.page_row_count)}
{$total_page_str = $total_page|string_format:"%d"}

件数：{$paging_data.total_row_count}<br />
全ページ数：{$total_page}<br />
表示ページ：{$paging_data.current_page}<br />

<hr />

<table>
    <tr>
        <th width="15%">日付</th>
        <th width="20%">URL</th>
        <th width="20%">タイトル</th>
        <th width="45%">説明</th>
    </tr>

    {foreach from=$rss_data.rows item=row}
    <tr>
        <td>{$row.date}</td>
        <td>
            <a href="{$row.url}" target="_blank">{$row.url}</a>
        </td>
        <td>{$row.title}</td>
        <td>{$row.description}</td>
    </tr>
    {/foreach}
</table>
{/if}

<hr />

{if $total_page}

{$prev = $paging_data.current_page - 1}
{$prev_str = $prev|string_format:"%d"}
{$next = $paging_data.current_page + 1}
{$next_str = $next|string_format:"%d"}

{* 先頭ページへ *}
<span>{if 1 <= $prev} <a href="{$paging_data.url|replace:"%d":1}">&lt;&lt;</a>{else}&lt;&lt;{/if}</span>

{* 前ページへ *}
<span>{if 1 <= $prev}<a href="{$paging_data.url|replace:"%d":$prev_str}">&lt;</a>{else}&lt;{/if}</span>

{* 各ページへ。前後３ページ *}
{$start_page = max(1, $paging_data.current_page - 3)}
{$end_page = min($total_page, $paging_data.current_page + 3)}

{for $page=$start_page to $end_page}
{$page_str = $page|string_format:"%d"}
<span>{if $page != $paging_data.current_page}<a href="{$paging_data.url|replace:"%d":$page_str}">{$page}</a>{else}{$page}{/if}</span>
{/for}

{* 次ページへ *}
<span>{if $next <= $total_page}<a href="{$paging_data.url|replace:"%d":$next_str}">&gt;</a>{else}&gt;{/if}</span>

{* 最終ページへ *}
<span>{if $next <= $total_page}<a href="{$paging_data.url|replace:"%d":$total_page_str}">&gt;&gt;</a>{else}&gt;&gt;{/if}</span>
{/if}

</body>
</html>
