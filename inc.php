<?php

/** PHP設定 **/

define("DEBUG", TRUE);

if (DEBUG === TRUE) {
    // エラー出力
    ini_set('display_errors', "On");
    error_reporting(E_ALL & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_NOTICE);
}

// エラーログ
$ymd = date('Ymd');
ini_set("log_errors", "on");
ini_set("error_log", __DIR__ . "/logs/error." . $ymd . ".log");

/** セッション設定 **/

// セッション開始
session_start();

/** MySQL設定 **/

// データベース名
define("DB_NAME", "lamp_papyrus");

// データベースのユーザー名
define("DB_USER", "papyrus");

// データベースのパスワード
define("DB_PASSWORD", "qF3549V6");

// MySQL　ホスト名
define("DB_HOST", "localhost");

/** ページ設定 **/

// １ページ表示数
define("PAGE_ROW_COUNT", 10);
