<?php

namespace Lib;

trait Helper
{
    /**
     * FC2BLOG URLの分割
     *  URLを「http://(ユーザー名).blog(サーバー番号).fc2.com/blog-entry-(エントリーNo.).html」のパーツに分割
     *
     * @param $url URL
     * @return array ユーザー名, サーバー番号, エントリーNo
     */
    static function parse_fc2_blog_url($url)
    {
        if (preg_match_all("/http:\/\/([a-zA-Z0-9]{1,}).blog(|[0-9]{1,}).fc2.com\/blog-entry-([0-9]{1,}).html/",
            $url, $matches)) {

            $user_name = (string)$matches[1][0];
            $server_num = $matches[2][0] == "" ? 0 : (int)$matches[2][0];
            $entry_no = (int)$matches[3][0];
        } else {
            // 指定フォーマットと違うURLの場合、ユーザー名、サーバー番号取得不能
            if (preg_match_all("/blog-entry-([0-9]{1,}).html/", $url, $matches)) {
                $user_name = null;
                $server_num = 0;
                $entry_no = (int)$matches[1][0];
            } else {
                return array();
            }
        }

        return array(
            "user_name" => $user_name,
            "server_num" => $server_num,
            "entry_no" => $entry_no
        );
    }

    /**
     * 検索フォームのバリデーション
     *
     * @param $request フォームデータ
     * @return array エラーメッセージ
     */
    static function form_validation($request)
    {
        $error_messages = array();

        if (!empty($request["date"])) {
            if (!strptime($request["date"], "%Y-%m-%d")) {
                $error_messages["date"] = "形式が違います。YYYY-MM-DD形式で入力してください。";
            } else {
                list($Y, $m, $d) = explode('-', $request["date"]);
                if (checkdate($m, $d, $Y) !== TRUE) {
                    $error_messages["date"] = "存在しない日付です。";
                }
            }
        }

        if (!empty($request["url"])) {
            if (!filter_var($request["url"], FILTER_VALIDATE_URL)) {
                $error_messages["url"] = "URLを入力してください。";
            } elseif (mb_strlen($request["url"]) > 255) {
                $error_messages["url"] = "255文字以内で入力してください。";
            }
        }

        if (!empty($request["user_name"])) {
            if (mb_strlen($request["user_name"]) > 255) {
                $error_messages["user_name"] = "255文字以内で入力してください。";
            }
        }

        if (!empty($request["server_num"])) {
            if (!is_numeric($request["server_num"])) {
                $error_messages["server_num"] = "数値で入力してください。";
            }
        }

        if (!empty($request["entry_no"])) {
            if (!is_numeric($request["entry_no"])) {
                $error_messages["entry_no"] = "数値で入力してください。";
            }
        }

        return $error_messages;
    }
}
