<?php

class Fc2BlogRssQuery
{

    private $mysqli;

    /**
     * Fc2BlogRssQuery constructor.
     */
    function __construct()
    {
        try {
            $this->mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
            if ($this->mysqli->connect_errno) {
                throw new Exception($this->mysqli->connect_error);
            }

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * RSSデータをMySQLに保存
     *
     * @param $rss_data RSSデータ
     * @return array ステータス, メッセージ
     */
    function save_rss($rss_data)
    {
        $query = "INSERT INTO fc2_blog_rss(`url`, `date`, `user_name`, `server_num`, `entry_no`, `title`, `description`) VALUES ";
        $query_parts = array();

        foreach ($rss_data as $val) {

            $query_parts[] = sprintf("('%s', '%s', '%s', %s, %s, '%s', '%s')",
                $this->mysqli->real_escape_string($val["url"]),
                $this->mysqli->real_escape_string($val["date"]),
                $this->mysqli->real_escape_string($val["user_name"]),
                $this->mysqli->real_escape_string($val["server_num"]),
                $this->mysqli->real_escape_string($val["entry_no"]),
                $this->mysqli->real_escape_string($val["title"]),
                $this->mysqli->real_escape_string($val["description"])
            );
        }

        $query .= implode(',', $query_parts);

        // 重複レコードがある場合、タイトル、概要更新の可能性があるので更新
        $query .= ' ON DUPLICATE KEY UPDATE date = VALUES(date), title = VALUES(title), description = VALUES(description);';

        try {
            $this->mysqli->begin_transaction();
            $result = $this->mysqli->query($query);
            if (!$result) {
                throw new Exception($this->mysqli->errno . ": " . $this->mysqli->error);
            }

            $message = "FC2BLOG RSS DB登録成功 更新レコード数:" . $this->mysqli->affected_rows;
            $this->mysqli->commit();

            return array(
                "status" => TRUE,
                "message" => $message
            );

        } catch (Exception $e) {
            $this->mysqli->rollback();

            return array(
                "status" => FALSE,
                "message" => $e->getMessage()
            );
        }
    }


    /**
     * WHERE句を作成
     *
     * @param $date 日付
     * @param $url URL
     * @param $user_name ユーザー名
     * @param $server_num サーバー番号
     * @param $entry_no エントリーNo
     * @return string WHERE句
     */
    private function _join_where($date, $url, $user_name, $server_num, $entry_no)
    {
        $where_parts = array();

        if (!empty($date)) {
            $where_parts[] = sprintf("`date` >= '%s' AND `date` <= '%s'",
                $this->mysqli->real_escape_string($date . " 00:00:00"),
                $this->mysqli->real_escape_string($date . " 23:59:59")
            );
        }

        if (!empty($url)) {
            // MEMO: morita-toyscreation INDEXが効くように前方一致のみ
            $where_parts[] = sprintf("`url` LIKE '%s'",
                $this->mysqli->real_escape_string($url) . "%"
            );
        }

        if (!empty($user_name)) {
            $where_parts[] = sprintf("`user_name` = '%s'",
                $this->mysqli->real_escape_string($user_name)
            );
        }

        if (is_numeric($server_num)) {
            $where_parts[] = sprintf("`server_num` = %s",
                $this->mysqli->real_escape_string($server_num)
            );
        }

        if (is_numeric($entry_no)) {
            $where_parts[] = sprintf("`entry_no` >= %s",
                $this->mysqli->real_escape_string($entry_no)
            );
        }

        $where_parts[] = "`status` = 1";

        return " WHERE " . implode(" AND ", $where_parts);
    }

    /**
     * FC2BLOG RSS 検索データ取得
     *
     * @param $date 日付
     * @param $url URL
     * @param $user_name ユーザー名
     * @param $server_num サーバー番号
     * @param $entry_no エントリーNo
     * @param $page ページ番号
     * @return array RSSデータ
     * @throws Exception
     */
    private function _get_query($date, $url, $user_name, $server_num, $entry_no, $page)
    {

        $query = "SELECT * FROM `fc2_blog_rss`";
        $query .= $this->_join_where($date, $url, $user_name, $server_num, $entry_no);
        $query .= sprintf(" ORDER BY date DESC LIMIT %s, %s",
            ($page - 1) * PAGE_ROW_COUNT,PAGE_ROW_COUNT);

        $result = $this->mysqli->query($query);
        if (!$result) {
            throw new Exception($this->mysqli->errno . ": " . $this->mysqli->error);
        }

        $rows = array();
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * FC2BLOG RSS 検索カウント数取得
     *
     * @param $date 日付
     * @param $url URL
     * @param $user_name ユーザー名
     * @param $server_num サーバー番号
     * @param $entry_no エントリーNo
     * @return int カウント数
     * @throws Exception
     */
    private function _get_count_query($date, $url, $user_name, $server_num, $entry_no)
    {
        $query = "SELECT COUNT(*) AS count FROM `fc2_blog_rss`";
        $query .= $this->_join_where($date, $url, $user_name, $server_num, $entry_no);

        $result = $this->mysqli->query($query);
        if (!$result) {
            throw new Exception($this->mysqli->errno . ": " . $this->mysqli->error);
        }

        $row = $result->fetch_assoc();

        return $row["count"];
    }

    /**
     * FC2BLOG RSS データ取得
     *  検索データ取得
     *  検索カウント数取得
     *
     * @param $date 日付
     * @param $url URL
     * @param $user_name ユーザー名
     * @param $server_num サーバー番号
     * @param $entry_no エントリーNo
     * @param $page ページ番号
     * @return array ステータス, 検索データ取得, 検索カウント数取得
     */
    function get_rss($date, $url, $user_name, $server_num, $entry_no, $page)
    {
        try {
            $rows = $this->_get_query($date, $url, $user_name, $server_num, $entry_no, $page);
            $count = $this->_get_count_query($date, $url, $user_name, $server_num, $entry_no);

            return array(
                "status" => TRUE,
                "rows" => $rows,
                "count" => $count,
                "message" => "FC2BLOG RSS 取得成功"
            );
        } catch (Exception $e) {
            return array(
                "status" => FALSE,
                "rows" => array(),
                "count" => 0,
                "message" => $e->getMessage(),
            );
        }
    }

    /**
     * Fc2BlogRssQuery destructor.
     */
    function __destruct()
    {
        mysqli_close($this->mysqli);
    }
}
