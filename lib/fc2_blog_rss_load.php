<?php

require_once __DIR__ . "/helper.php";

use Lib\Helper;

class Fc2BlogRssLoad
{

    private $rss_data;

    /**
     * Fc2BlogRssLoad constructor.
     */
    function __construct()
    {
        $this->rss_data = array();
    }

    /**
     * FC2BLOG RSS データセット
     *
     * @return array ステータス, メッセージ
     */
    function set_rss()
    {
        $url = "http://blog.fc2.com/newentry.rdf";

        try {
            $xml = simplexml_load_file($url, "SimpleXMLElement", LIBXML_NOCDATA);

            if (!$xml) {
                throw new Exception("FC2BLOG RSS取得失敗");
            }

            foreach ($xml as $val) {

                // URLを「http://(ユーザー名).blog(サーバー番号).fc2.com/blog-entry-(エントリーNo.).html」のパーツに分割
                $parses = Helper::parse_fc2_blog_url($val->link);

                // 適正URL以外は除外
                if (empty($parses)) {
                    continue;
                }

                $this->rss_data[] = [
                    "user_name" => $parses["user_name"],
                    "server_num" => $parses["server_num"],
                    "entry_no" => $parses["entry_no"],
                    "date" => (string)$val->children("dc",true)->date,
                    "url" => (string)$val->link,
                    "title" => (string)$val->title,
                    "description" => (string)$val->description,
                ];
            }

            return array(
                "status" => TRUE,
                "message" => "FC2BLOG RSS取得成功"
            );

        } catch (Exception $e) {
            return array(
                "status" => FALSE,
                "message" => $e->getMessage()
            );
        }
    }

    /**
     * FC2BLOG RSS データ取得
     *
     * @return array ステータス, メッセージ, RSSデータ
     */
    function get_rss()
    {
        if (empty($this->rss_data)) {
            $result = $this->set_rss();
            return array(
                "status" => $result["status"],
                "message" => $result["message"],
                "rss_data" => $this->rss_data
            );

        } else {
            return array(
                "status" => TRUE,
                "message" => "FC2BLOG RSS取得成功（取得済）",
                "rss_data" => $this->rss_data
            );
        }
    }
}
